import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        casoPrueba_reto3();
    }

    public static void casoPrueba_reto3() {
        Profesor objProfesor1 = new Profesor("Raúl", 1875000);
        // Añadir Cursos
        objProfesor1.addCurso(new Curso("Física"));
        objProfesor1.addCurso(new Curso("Química"));
        objProfesor1.addCurso(new Curso("Biología"));

        Profesor objProfesor2 = new Profesor("Pedro", 1755600);
        // Añadir Cursos
        objProfesor1.addCurso(new Curso("Historia"));
        objProfesor1.addCurso(new Curso("Sociales"));
        objProfesor1.addCurso(new Curso("Ética"));

        ArrayList<Profesor> profesores = new ArrayList<Profesor>();
        profesores.add(objProfesor1);
        profesores.add(objProfesor2);

        // Crear objeto colegio
        Colegio objColegio = new Colegio();

        ArrayList<Double> segSocial = objColegio.liquidarSegSocial(profesores);
        for (int i = 0; i < segSocial.size(); i++) {
            System.out.println(segSocial.get(i));
        }

    }

    public void casoPrueba_reto2() {
        Profesor objProfesor = new Profesor("Raúl", 1875000);
        // Añadir Cursos
        objProfesor.addCurso(new Curso("Física"));
        objProfesor.addCurso(new Curso("Química"));
        objProfesor.addCurso(new Curso("Biología"));

        // Crear objeto colegio
        Colegio objColegio = new Colegio();
        // Añadir profesor al colegio
        objColegio.contratarProfe(objProfesor);

        System.out.println(objColegio.liquidarNominaProfe(objProfesor));
    }
}
