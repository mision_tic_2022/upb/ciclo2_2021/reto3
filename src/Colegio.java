import java.util.ArrayList;

/************************************************************
 * Autor:
 * Fecha: 
 * Ciudad:
 * Descripción: Clase que me representa un colegio ...
 * MISIONTIC 2022
 * Reto 2
 *************************************************************/

public class Colegio{
    //Atributos
    private ArrayList<Profesor> profesores;

    //Constructor
    public Colegio(){
        //Inicializar arrayList
        this.profesores = new ArrayList<Profesor>();
    }

    //Getters and Setters

    public Profesor getProfesor(int pos) {
        return profesores.get(pos);
    }

    public int getSizeProfesores(){
        return profesores.size();
    }

    public void setProfesor(int pos, Profesor profesor) {
        this.profesores.set(pos, profesor);
    }

    //Acciones

    public ArrayList<Double> liquidarPrestacionesProfe(ArrayList<Profesor> profesores){
        ArrayList<Double> arraySalarios = new ArrayList<Double>();
        //Iterar el array
        for(int i = 0; i < profesores.size(); i++){
            double prima = (profesores.get(i).getSalario() * 0.0833);
            double cesantias = (profesores.get(i).getSalario() * 0.0833);
            double vacaciones = (profesores.get(i).getSalario() * 0.0416);
            double salario = prima + cesantias + (cesantias*0.012) + vacaciones;

            arraySalarios.add(salario);
        }
        return arraySalarios;
    }

    public double liquidarNominaProfe(Profesor profesor){

        int incremento_salarial = 0;
        if(profesor.getSizeCursos() > 1){
            incremento_salarial = (profesor.getSizeCursos() - 1) * 180000;
        }
        //Actualizar el salario base
        profesor.setSalario( profesor.getSalario() + incremento_salarial);
        //Obtener las deducciones
        double deducciones = profesor.getSalario() * 0.08;

        double respuesta = (profesor.getSalario() - deducciones);

        return respuesta;
    }

    public void contratarProfe(Profesor profesor){
        profesores.add(profesor);
    }

    public ArrayList<Double> liquidarSegSocial(ArrayList<Profesor> profesores){
        ArrayList<Double> seguridadSocial = new ArrayList<Double>();
        //recorrer los profesores recibidos
        for(int i = 0; i < profesores.size(); i++){
            Profesor profesor = profesores.get(i);
            //this.liquidarNominaProfe(profesor);
            //double total_devengado = profesor.getSalario();
            double total_devengado = profesor.getSalario() + ((profesor.getSizeCursos()-1)*180000);
            double salud = total_devengado * 0.085;
            double pension = total_devengado * 0.12;
            double riesgos_laborales = total_devengado * 0.00522;

            double sumatoria = salud + pension + riesgos_laborales;
            seguridadSocial.add(sumatoria);
        }

        return seguridadSocial;
    }

}